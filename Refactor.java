package aaa;

import java.util.Scanner;

/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */


/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactor {
	//creada variable estática
	//añadido nombre relevante
	final static int maxAlumnos = 10;
	
	//creado método estático fuera del main
	//modificado para abarcar problemas genericos
	//añadido nombre relevante
	public static double media(int vector[])
	{
		double aux = 0;
		//recorre todo el vector, sea de la
		//longitud que sea
		for(int i = 0; i< vector.length; i++) 
		{
			aux += vector[i];
		}
		return (aux / vector.length);
	}
	
	
	public static void main(String[] args) 
	{
		//creado scanner como variable local
		//del main
		Scanner sc = new Scanner(System.in);
		
		//nombre relevante
		int notas[] = new int[maxAlumnos];
		
		//iterador 'n' renombrado a 'i'
		// y encapsulado al ámbito del bucle
		for(int i = 0; i < notas.length; i++)
		{
			//informacion extra para el usuario
			System.out.println("Introduce nota media de alumno " + (i+1));
			notas[i] = sc.nextInt();
		}	
		
		//feedback comprehensible para el usuario
		System.out.println("El resultado es: " + media(notas));
		//se cierra el scanner al final del método
		sc.close();
	}

}