package aaa;
/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */


import java.lang.reflect.Array;
import java.util.Scanner;

/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refac {
	final static String cad = "Bienvenido al programa";
	final static String[] dia = new String[7];
	static {
		dia[0] = "Lunes";
		dia[1] = "Martes";
		dia[2] = "Miercoles";
		dia[3] = "Jueves";
		dia[4] = "Viernes";
		dia[5] = "Sabado";
		dia[6] = "Domingo";
	}
	                               
	final static int siete = 7;
	final static int dieciseis = 16;
	final static int veinticinco = 25;
	final static int cinco = 5;
	final static int cero = 0;
	final static int tres = 3;
	final static int uno = 1;
	
	public static void recorrer_array(String vector[])
	{
		for(int dia = 0; dia < vector.length; dia++) 
		{
			System.out.println("El dia de la semana en el que te encuentras ["
					+ (dia+1) + "] es el dia: " + vector[dia]);
		}
	}
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println(cad);
		
		System.out.println("Introduce tu dni");
		String dni = sc.nextLine();
		
		System.out.println("Introduce tu nombre");
		String nombre = sc.nextLine();
		
		int numA = siete;
		int numB = dieciseis;
		int numC = veinticinco;
		
		if( (numA > numB) || (numC % cinco != cero)
				&& ((numC * tres - uno) > (numB / numC)))
		{
			System.out.println("Se cumple la condición");
		}
		
		numC = numA + (numB * numC) + (numB / numA);
		
		
		recorrer_array(dia);
		
		sc.close();
	}
}
