package principal;

import java.io.File;
import java.util.Scanner;

public class Principal {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];
	private final static String ruta = "src\\palabras.txt";
	
	
	public static void main(String[] args) {

		String palabraSecreta;
		extraerCosas();
		palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];
		
		Scanner input = new Scanner(System.in);

		char[][] caracteresPalabra = inicializarJuegor(palabraSecreta);

		String caracteresElegidos = "";
		int fallos;
		boolean acertado;
		System.out.println("Acierta la palabra");
		do {

			pintarMenu(caracteresPalabra, caracteresElegidos);
			caracteresElegidos += input.nextLine().toUpperCase();
			fallos = 0;

			fallos = encuentraFallos(caracteresPalabra, caracteresElegidos, fallos);

			pintarAhorcado(fallos);

			if (fallos >= FALLOS) {
				System.out.println("Has perdido: " + palabraSecreta);
			}
			acertado = true;
			for (int i = 0; i < caracteresPalabra[1].length; i++) {
				if (caracteresPalabra[1][i] != '1') {
					acertado = false;
					break;
				}
			}
			if (acertado)
				System.out.println("Has Acertado ");

		} while (!acertado && fallos < FALLOS);

		input.close();
	}
	/**
	 * @author me
	 * @since tomorrow
	 * @rekt yes
	 * Esta documentacion solo sirve para saber si se
	 * hacer documentacion
	 * @param caracteresPalabra palabara que adivinar
	 * @param caracteresElegidos Las letras que queremos adivinar en el ahorcador
	 * @param fallos ek numero de fallos hasta ahora
	 * @return devuelve el numero de fallos total tras intentar los caracteres en caracteresElegido
	 */
	private static int encuentraFallos(char[][] caracteresPalabra, String caracteresElegidos, int fallos) {
		boolean encontrado;
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			encontrado = false;
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = '1';
					encontrado = true;
				}
			}
			if (!encontrado)
				fallos++;
		}
		return fallos;
	}

	private static char[][] inicializarJuegor(String palabraSecreta) {
		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];
		return caracteresPalabra;
	}

	private static void pintarMenu(char[][] caracteresPalabra, String caracteresElegidos) {
		System.out.println("####################################");

		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();

		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
	}

	private static void extraerCosas() {
		File fich = new File(ruta);
		Scanner inputFichero = null;

		try {
			inputFichero = new Scanner(fich);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fich != null && inputFichero != null)
				inputFichero.close();
		}
	}

	private static void pintarAhorcado(int fallos) {
		switch (fallos) {
		case 1:

			System.out.println("     ___");
			break;
		case 2:

			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
			break;
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
			break;
		}
	}

}